

let intNumber = Number(prompt("Input Number:"));

console.log("The number you provided is: " + intNumber);

for( i =  intNumber; i > 0 ; i-- ){
    if ( i <= 50 ){
        console.log("The current value is at " + i + " . Terminating the loop.");
        break;
    }

    if( i % 10 === 0 ){
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }

    if( i % 5 === 0 ){
        console.log(i);
    }
}

// ---------------------------------------------

let strWord = "supercalifragilisticexpialidocious";
let strConsonant = "";

strWord=strWord.toLowerCase();

for ( i = 0 ;  i < strWord.length ; i++ ){

    if (strWord[i]=="a" || strWord[i]=="e" || strWord[i]=="i" || strWord[i]=="o" || strWord[i]=="u"){
        continue;
    } else {
        strConsonant = strConsonant + strWord[i];
    }
}

console.log(strWord);
console.log(strConsonant);